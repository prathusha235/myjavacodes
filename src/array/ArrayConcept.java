package array;

public class ArrayConcept {

	//we can store huge no of values in a single variable.
	//1.declaring array
	//2.Assigning the value
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	// Single dimension Array	
		int [] x;
		int y [];
		int[] z;
		
		x = new int[5];
		x[0]=400;
		x[1]=22;
		x[2]=400;
		x[3]=22;
		x[4]=400;
		System.out.println(x.length);
//		x[5]=22;	// will through arrayIndexOutofBound exception
//		x[6]=400;
//		x[7]=22;
//		
	int k=0;
	while(k<x.length) {
		System.out.println();
		k++;
	}
		
	String name [] = new String [3];
	name [0] = "ABC";
	
	
//	2Dimension Array
	
	int number [][]= new int [2][2];
	
	number[0][0]=40;
	number[0][1]=50;
	number[1][0]=60;
	number[1][1]=70;
	
	System.out.println("The value at zeroth index is: "+number[0][0]);
	
	int size = number.length;
	System.out.println("Length of the Array is: "+size);
	for(int i=0; i<size; i++) {
		for (int j=0; j<size; j++)
			System.out.println("The number is: "+ number[i][j]);
	}
	
	int number1 [][] = {
			
			{12,13,14,66,90,98},
			{40,43,45,7},
			{3,5,9,1},
			{33,54,21,23,654}
	};
	
	int arrsize = number1.length;
	System.out.println(arrsize);
	
	for(int i=0; i<arrsize; i++) {
		for (int j=0; j<4; j++) {
			System.out.print(number1[i][j]+" ");
	}
	System.out.println(" ");
	
//	Enhanced for loop for array
	int  x1[]= {23,55,43,26};
	int y1[]= {2,1,5,9,3,4};
	
	for (int f:x1) {
		System.out.println(f);
	}
	for (int d:y1) {
		System.out.println(d);
		
	}
	
	int a2[][]= {
			{9,7,8,5,3},
			{33,55,22},
			{12,19,14,13,17,15,18}
	};
	for(int i1[]:a2) {
		for(int j1:i1) {
		System.out.print(j1+" ");	
		}
		System.out.println("");
		}
	
	}
	}
	

	

}
