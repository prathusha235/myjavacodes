package OopsConcept;

public class TestCar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// this is called static polymorphism or complie polymorphism 
		Bmw b = new Bmw();
		b.start(); 
		b.refeil(); // overridden frm parent class
		b.safty(); // overridden from own method
		b.stop(); // overridden from parent class
		
	//Car c = new Car();
	//c.start();
	//c.stop();
	//c.refeil();
	}

}
