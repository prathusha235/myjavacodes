package OopsConcept;

public class Bmw extends Car {
	
	// when same method present in parent class as well as in child class is called methos overriding
	public void start() {
		System.out.println("Bmw start");
	}
	
	public void safty() {
		System.out.println("Bmw safty");
	}
}

