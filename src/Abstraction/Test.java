package Abstraction;

public class Test {

	public static void main(String[] args) {

		SbiBank obj = new SbiBank();
		
		obj.openaccount();
		obj.loan();
		
		IciciBank obj1 = new IciciBank();
		obj1.loan();
		obj1.openaccount();
	}

}
