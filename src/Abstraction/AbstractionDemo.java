package Abstraction;

public abstract class AbstractionDemo {

	public abstract void method1() ; // Abstract Method
	
	public abstract void method2(); //Abstract Method
	
	public void method3() {}  //Concrete Method
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Abstraction is hiding  implementation logic.
		//using the Abstract class we can achieve "partial abstraction" 
		//To achieve the abstract we have two ways
		// 1.Abstract class
		//2.Interface
		// In Abstract class at least one method must be Abstract(without body) Only method implementation no declaration.
		 //We can have Multiple abstract methods in Abstract class there no restrictions as well as we can have multiple Concrete methods.
		//*** We can't create the object of Abstract class.
		//
//		
//		
	}

}


