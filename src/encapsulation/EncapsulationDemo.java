package encapsulation;

public class EncapsulationDemo{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Binding data and Method together as a single Unit is called Encapsulation.j
		//If anyone wants to use the data member then they need to do it by calling the method 
		// they can't access the data member directly.
		//To achieve it we have two machanism 
		//1.getter
		//2.Setter
		//Here variables are kept as private data member so no one can use it.
		//Benefits of Encapsulation is:
		//1.user will not have a access about the internal implementation a class can have total control
		// that what being stored 
		//We can maintain a log inside a method when someone uses the log will be printed.
		EncapsulationDemo2 obj = new EncapsulationDemo2();
		System.out.println(obj.getName());
		obj.setName("Hello");
		System.out.println(obj.getName());
		
	}

}

class EncapsulationDemo2 {
	private int roll;
	private String name;
	int x;
	public int getRoll() {
		
		System.out.println("user accessing the roll number");
		return roll;
	}
	public void setRoll(int roll) {
		System.out.println("user is trying to change the Roll number");
		this.roll = roll;
	}
	public String getName() {
		System.out.println("User Accessing the Name ");
		return name;
	}
	public void setName(String name) {
	System.out.println("User is trying to change the Name");
		this.name = name;
	}
	

}



