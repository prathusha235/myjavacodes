package inheritance;

public class MultilevelInheritance1 {
	

	public static void main(String[] args) {
		
		BabyDog obj = new BabyDog();
		obj.bark();
		obj.eat();
		obj.weep();

	}

}


class Animal {
	void eat() {
		System.out.println("animal eating");
	}
}

class Dog extends Animal {
	
	void bark() {
		System.out.println("barking");
	}
}

class BabyDog extends Dog {
	
	void weep() {
		
		System.out.println("weeping");
	}
	
}
