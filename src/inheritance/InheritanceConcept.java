package inheritance;

public class InheritanceConcept {
	
	// Deriving new new class from existing class so that the new class can aquire the feature from the existing class it's called inheritance.
	//has a relationship--- create an object of parent class into a child class and access a parent class member into child class.
	// is a relationship--- making relationship b/w to class using extends keyword this is also known as parent and child relationship.
    //why we use inheritance 
	 // 1. code reusability
	  // 2. method overriding
	// Extends keyword for creating the parent child relationship
	// at a time we can inheririte only one class 
	//a class can not be parent of interface
	// data member-- private member of the parent class can't be inherited.
	//public, protected, default of the 
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
