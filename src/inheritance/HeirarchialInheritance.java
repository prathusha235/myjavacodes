package inheritance;

public class HeirarchialInheritance {

	public static void main(String[] args) {

		Parrot obj1 = new Parrot();
		
		obj1.fly();
		obj1.Nest();
		
		
		Eagle obj2 = new Eagle();
		obj2.fly();
		obj2.eat();
		
	}

}

class Bird {
	void fly() {
		System.out.println(" I am flying");
	}
}

class Parrot extends Bird {
	void Nest() {
		System.out.println("I am from Parent class");
	}
	
}
class Eagle extends Bird {
	
	void eat() {
		System.out.println(" Eagle is eating");
	}
	
}