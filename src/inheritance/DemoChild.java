package inheritance;

public class DemoChild extends DemoParent{
//	SIngle level inheritance
	int number5 = 55;
	
	public DemoChild() {
		System.out.println("Constructor from child class");
	}
	public static void main(String[] args) {
		
		DemoChild obj = new DemoChild();
		System.out.println(obj.number1);
		System.out.println(obj.number5);
		
		DemoParent obj1 = new DemoChild();
		System.out.println(obj1.number4);
		
		DemoParent obj2 = new DemoParent();
		obj2.myMethod1();
		System.out.println(obj2.number3);
		
			
	}

}
