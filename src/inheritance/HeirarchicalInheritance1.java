package inheritance;

public class HeirarchicalInheritance1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Cat1 c = new Cat1();
		c.eat();
		c.meow();
		
		Dog1 d = new Dog1();
		d.bark();
		d.eat();
	}

}


class Animal1 {
	
	void eat() {
		System.out.println("eating");
}
}

class Cat1 extends Animal1 {
	
	void meow() {
		
		System.out.println("cat is meowing");
	}
}

class Dog1 extends Animal1 {
	
  void bark() {
  System.out.println("barking");
	}
}

