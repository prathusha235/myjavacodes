package inheritance;


public class MultiLevelInheritance {

	public static void main(String[] args) {
		
		AddSubMul obj = new AddSubMul();
		obj.x = 10;
		obj.y = 5;
		obj.sum();
		System.out.println(obj.result);
		obj.sub();
		System.out.println(obj.result);
		obj.mul();
		System.out.println(obj.result);
		
	}
	
	
	
}


class Addition {
	
	int x,y,result;
	public void sum() {
		result= x+y;
		
	}	
}

class AddSub extends Addition {
	
	public void sub() {
		result=x-y;
	}
}
class AddSubMul extends AddSub {
	public void mul() {
		result= x*y;
	}
}