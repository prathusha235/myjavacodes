package polymorphism;

public class DynamicPolymorphism {

	public static void main(String[] args) {

//	Method overriding >	Run time polymorphism > Late binding > Dynamic polymorphism
		Child obj = new Child();
		obj.run();
	}

}

class Parent {
	
	public void run() { //overridden method
		System.out.println("I am from the parent class");
	}
}

class Child extends Parent {
	
	public void run() { //overriding method
		System.out.println("I am from the child class");
	}
	
	public void jump() {
		System.out.println("Jumping from Child class");
	}
}
