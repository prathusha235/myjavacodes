package polymorphism;

public class StaticPolymorphism {

	// Static Polymorphism:
	//when we do the mehtod overloading method name is same but the parameter is diff here we achieve 
	// method overloading happens at the time of compalation this is also callesd as compiletime polymorphism or early binding.
	public void add() {
		System.out.println("adding");
	}
	public void add(int x, int y) {
		System.out.println(x+y);
	}
	
	public void add(int i, float f) {
		System.out.println(i+f);
	}
	
	public static void main(String[] args) {
		
//	Method overloading > Early binding > Static binding > compile time polymorphism	
		StaticPolymorphism obj = new StaticPolymorphism();
		obj.add();
		obj.add(2,4);
		obj.add(3,5.6f);
		
	}

}
