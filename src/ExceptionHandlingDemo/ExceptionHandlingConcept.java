package ExceptionHandlingDemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ExceptionHandlingConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*
 * Exception is an abnormal condition which may occur during execution and
 *  disrupts the normal flow of the program.
 *  Exception Handling: This is a mechanism to handke the exception using try, catch keyword.
 *  Inside the Exception we have two types of exception
 *  1. checked Exception:Is compile time Exception this exception we come to know to at the time of compiling.
 *  2.Unchecked Exception: It is Run time Exception, the exception we come to at time time of execution.
 *  Error: basically indicates the serious problems which are beyond control like out of memory error, stack over flow,
 *  virtual machine error.
 *  Default Exception Handling:  
 */
		
		
		/*Throwable
		 * 	Exception:  exceptions are caused by our program and it is recoverable.
		 * 
		 * 		Checked
		 * 			-IOException,SQLException, ClassNotFoundException
		 * 
		 * 
		 * 
		 * 
		 * 		Unchecked
		 *          -run time Exception like Arithmetic Exception, Number format exception, IndexoutofBound Exception
		 *          
		 * 
		 * 
		 * 
		 * 	Errors
		 *       -Error occurs lack of System resources.
		 *      
		 */
		
		{
//			Class.forName("");
//			This will through class not found exception
			
		}
		
		try
		{
			Class.forName("MyClass");
		}
		catch(ClassNotFoundException ex)
		{
			System.out.println("It is ClassNotFoundException");
		}
		
		File abc = new File("c://xyz.txt");
		try {
			FileInputStream f = new FileInputStream(abc);
			
		} 
		
		catch (Exception mm) {
			System.out.println("This is file not found exception");
		}
		
		
	
	}

}
