package operators;

public class OperatorBasics {

	public static void main(String[] args) {
//		Operator is symbol that performs operation on some variable or value
//		Arithmetic, Butwise, Relational, LoLogical
		//Arithmetics- these are 5 (+, -, *, /, %) needs two operand so its called bitwaise operator
		
		int x= 5/2;
		int y = 5%2;
		
		System.out.println(x);
		System.out.println(y);
		
		
		System.out.println(x++);   //2.......................3
		System.out.println(++x);  //  4
		
		System.out.println(x--);  // 3	4-----------------------------3
		System.out.println(--x);  // 2
		
//		Assignment operator =  it is used to store some value into variable
//		z=10,	z1=z,	z2=z+z1
		int z=10;	//direct assignment
		System.out.println(z);	//10
		
		int z1=z;	//value of variable into another variable
		System.out.println(z1);	//10
		
		int z2=z+z1;	//value of expression
		System.out.println(z2);	//20
		
//	There are 6 relational operator
//		Logical operator 		&&, ||,  !
		
		int p=20, q=50, r=20;
		if (p==r || p<r) {
			System.out.println("P and R Both are samne");
		}

		if (p==r && p<q) {
			System.out.println("Example && Operator");
		}
		
		if (!(r==20)) {
			System.out.println("TESTtttttttt");
		}
		
//		Member operator (.) This operator tells about member of the package or class
		
//		new operator is used to create the object of class
		
	}

}
