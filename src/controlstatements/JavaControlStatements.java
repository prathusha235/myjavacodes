package controlstatements;

public class JavaControlStatements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x=11;
		
		
//if...else
		if (x==10) {
			System.out.println("the value of x = "+x);
		}
		else if (x>10){
			System.out.println("Inside else if part");
		}
		else
		{
			System.out.println("We are in else part");
		}
		
	for(int X=1; X<=11 ; X++) {
		
		System.out.println(X);

	}

	//Table of 2
	
	
	for(int i=1; i<= 10; i++ ) {
		int abc =2*i;		
		System.out.println(abc);
	}
	
//	* * *
//	* * * 

	for(int i=1 ; i<=3; i++) {
     for(int j=1; j<=3; j++) {
    	 System.out.print("*");
     }
     System.out.println("");
	}
	
	
	
//	*
//	* *
//	* * *
	
	for(int i=1; i<=10; i++) {
		for(int j=1; j<=i; j++) {
			System.out.print("*");
	     }
	     System.out.println("");
		}
	
	/*	12345
	 *  12345
	 *  12345
	 *  12345
	 *  12345
	 */
	
	
	/* 1
	 * 1 2
	 * 1 2 3
	 * 1 2 3 4
	 * 1 2 3 4 5
	 */
	
	/* A
	 * A B
	 * A B C
	 * A B C D
	 * A B C D E
	 */
	
	int i, j;
	for( i=1; i<=5; i++) {
		for(j=1; j<=i; j++) {
			System.out.print(j);
		}
		System.out.println();
	
		}
	
	for( i=1;i<=5;i++)
	{
		for( j=1;j<=5;j++) {
			System.out.print(j);
			
		}
		System.out.println();
	}
	

	
	for(char p = 'A'; p<= 'G'; p++)
	{
		for(char p1 = 'A';p1<=p; p1++) {
			System.out.print(p1);
			
		}
		System.out.println();
	}
	
	for (char k=65; k<=80; k++) {
		for(char l=65; l<=k; l++) {
			System.out.print(l);
		}
		System.out.println();
	}

	}
	}

