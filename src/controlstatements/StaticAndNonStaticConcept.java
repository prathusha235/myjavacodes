package controlstatements;

public class StaticAndNonStaticConcept {

	
//	STATIC BLOCK==================================
/*
 * 	A block of statement declared as static is called static block
 *  JVM executes static block on highest priority even before main method
 */
	
	public static void main(String[] args) {
		System.out.println("I am from main method");
		
		StaticMethodConcept.Abc();  //static method directly calling using class name.methodname
		System.out.println(StaticMethodConcept.x);
		
		StaticMethodConcept obj = new StaticMethodConcept();
		obj.sum();
		System.out.println(obj.f);
		
	}
	
	
	static {
		System.out.println("I am from static block 1");
	}

	
	static {
		System.out.println("I am from static block 2");
	}
	
	static {
		System.out.println("I am from static block 3");
	}
	
//	STATIC METHODS
/* 1 static method belongs to class rather than the object.
 * static method are called classname.methodname.
 * JVM first execute the static methods and then creates the objects.
 *we don't need to create the object of the class to call the static method
 *  Static method can access static data member and change number of it  directly.
 *  
 */
	
	
//STATIC VARIABLE
	/*
	 * static variable are also called as class variable.
	 * JVM provide default value to static variable.
	 * Static variable can directly accessed by static and instance method.
	 * Static variables are created at the time of class loading and distrayed at the time of class unloading.
	 * 
	 */
	
	// STATI keyword allowed with variable,method,block, innerclass, inner Interface
	
	// STATIC Keyword not allowed with outer class, outer interface, constructor.
	
	
}
