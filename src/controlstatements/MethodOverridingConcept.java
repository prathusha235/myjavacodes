package controlstatements;

public class MethodOverridingConcept {
	

	public static void main(String[] args) {

		AA a= new AA();
		a.myMethod();
		
		BB b  = new BB();
		b.myMethod();
	}

}

class AA {
	 int x=10;
	 public void myMethod() {
		
		System.out.println("Method from class AA"); 
		 
	 }
}

class BB extends AA{
	int x=20;
	public void myMethod() {
		
		
		System.out.println("Method from class BB"); 
		System.out.println(x);
		System.out.println(super.x);
		 
	 }
}