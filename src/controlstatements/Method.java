package controlstatements;

public class Method {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Method is a set of statements to perform some action.
		//it consists name, return type. access modifier and parameters.
		//method may return something or may not.
		//
		Method obj = new Method();
		obj.myMethod1();
		String result=obj.parameterMethod(6);
		System.out.println(result);
	}
	
	public void myMethod() {
		System.out.println("Non parameterized method");
	}

	public int myMethod1() {
		System.out.println("Non parameterized method with Retun");
		return 0;
	}
	
	public void myMethod2(String s) {
		System.out.println(s);		
	}
	
	public String myMethod3(String s) {
		return s;
		
	}
	
	public String parameterMethod(int age) {
		if(age>=18) {
	
			return "adult";
		}
		
		else {
			
			return "minor";
			
		}
	
		
	}

}


