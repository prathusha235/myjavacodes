package controlstatements;

public class ConstructorConcept1 {
	
	String name;
	int age;
	
	public ConstructorConcept1() {
		
	System.out.println("Default Constr");
	
	}
	
	public ConstructorConcept1(int i) {
		System.out.println("1parameter Constructor");
		System.out.println(i);
	}
	
	public ConstructorConcept1(int i, int j) {
		System.out.println("2parameter Constructor");
		System.out.println(i + " " + j);
	}
	
	// this is keyword is used for when we have to initialize to  class variables with local variable
	
	public ConstructorConcept1(String name, int age) {
		
		this.name= name; //this.class variable= local variable
		this.age= age;
			
//		name = name;
//		age= age;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		ConstructorConcept1 con = new ConstructorConcept1();
//		
//		ConstructorConcept1 con1 = new ConstructorConcept1(23);
//		
//		ConstructorConcept1 con2 = new ConstructorConcept1(23, 30);
		ConstructorConcept1 con3 = new ConstructorConcept1("Tom", 25);
		System.out.println(con3.name);
		System.out.println(con3.age);
	}

}
