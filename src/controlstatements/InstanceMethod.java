package controlstatements;

public class InstanceMethod {

	// * Instance Method are method which acts upon instance variable.
	// To call the instance mathod we need to create the object.
	// The benifite of instance method is it can access instance variable as well as static varicable
	
	static int i,j;
	int k,l;
	
	public void sum() {  //instance method
		i= 10;
		j=23;
		k=12;
		l=5;
		
	}
	
	public static void sub() {  // Static method
		i= 12; // Static method can only access the static variables.
		j=25;
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
