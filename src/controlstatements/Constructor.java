package controlstatements;

public class Constructor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 // constructor is similar to a method that is used to initiasled instance of a variable.
		// constructor name should be same as the class name
		// if constructor doesn't have any parameter is called default constructor
		// only one default class is allowed for class
		//if constructor has parameter is called parameterized constructor.
		// constructor doesn't return any value 
		// either void or any value.
	// A constructor is automatically is called and executed at the time of object creation
		// A constructor is called only once per object.Abc obj = new Abc();
		// Access modifiers is allowed with the constructor.
		
		Abc obj = new Abc();
		Abc obj1 = new Abc("Hello Java", 45);
		
	}

}
class Abc{
	
	 Abc() {	//non parameterized constructor
		 int x=1;
		 System.out.println("ABC cunstructor"+x);
	 }
	 
	 Abc(String s){		// one parameter constructor or argumented constructor
		 System.out.println(s);
	 }
	 
	 Abc(String s, int i){		//Two parameter
		 System.out.println(s);
		 System.out.println(i);
	 }
	 
	 Abc(int i, String s){		//same arguments with sequence change is allowed with constructor overloading
		 System.out.println(s);
		 System.out.println(i);
	 }
	 
	 {
		System.out.println("Initializer Block"); 
	 }
	 
	 static {
		 System.out.println("I am from Static Block");
	 }
}