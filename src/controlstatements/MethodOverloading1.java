package controlstatements;

public class MethodOverloading1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		 MethodOverloading1 obj = new MethodOverloading1();
		 obj.sum();
		 obj.sum(10);
		 obj.sum(10, 20);
		 

	}
	

	
	// Can we overload main method: yes we can overload a main method 
	
	// you can't create a method inside a method
	//duplicate method--- i.e method name is same and same arguments are not allowed
	//Method overloading--- i.e same method name with different argue or input parameters and different data types with in the same class.
	
	
	public void sum() {
		System.out.println("Default method");
	}

	public void sum(int i) {
		System.out.println("1 parameter method");
		System.out.println(i);
	}
	public void sum(int i, int j) {
		System.out.println("2parameter method");
		System.out.println(i+j);
	}
	
}
