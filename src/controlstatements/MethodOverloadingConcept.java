package controlstatements;

public class MethodOverloadingConcept {
	
	// when the method name is name same with iff agru or diff para within the class is called method over loading

	public void m1() {
		
	}
	
	public void m1(int x) {
		
	}
	
	public void m1(int x, int y) {
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
